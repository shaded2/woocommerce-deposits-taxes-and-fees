# README #

### What is this repository for? ###

This is an extention to the woocommerce-deposits plugin. It is intended to change the way taxes and fees are handled by the original woocommerce-deposits extension


### Dependencies ###

* Wordpress
* Woocommerce
* Woocommerce deposits
* WooCommerce Product Fees (optinal dependency)

### Requirements ###

Turn on by going to woocommerce settings > products > deposits screen. There should be an option to "include taxes and fees in deposit balance"
Choosing yes should allow the customer to see full balance including fees and taxes on all invoices, screens and emails

### How to contribute ###

* Create a new git branch from master
* Push new branch with commits and descriptive commit message to bitbucket
* Create a pull request to master in bitbucket with descriptive pull request title
* Wait for merge or comments. 





